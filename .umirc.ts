import { defineConfig } from 'umi';

export default defineConfig({
  locale: { antd: true },
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    { path: '/', component: '@/pages/index' },
    { path: '/login', component: '@/pages/login' },
    { path: '/demande', component: '@/pages/demandeForm' },
    { path: '/list', component: '@/pages/demandesList' },
  ],
});
