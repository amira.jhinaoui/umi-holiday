import { IDemande, IDemandeCreate, DEMANDE_CREATE, DEMANDE_CREATE_SUCCESS, IDemandeCreateSuccess, IDemandeCreateError, DEMANDE_CREATE_ERROR, IDemandes, IDemandeGetAll, DEMANDE_GET_ALL, IDemandesGetAllSuccess, DEMANDE_GET_ALL__SUCCESS, IDemandesGetAllError, DEMANDE_GET_ALL_ERROR } from './types';
import { IErrors } from '@/utils/types';

/**
 * Create a Request Action
 */
export const demandeCreate =(request : IDemande): IDemandeCreate => ({
    type: DEMANDE_CREATE,
    payload:request,

});

export const demandeCreateSuccess =() : IDemandeCreateSuccess => ({
    type: DEMANDE_CREATE_SUCCESS,
});

export const demandeCreateError =(errors:IErrors) : IDemandeCreateError => ({
    type: DEMANDE_CREATE_ERROR,
    payload: errors,
});

/**
 * Get All Requests Action
 */
export const demandesGetAll = ():IDemandeGetAll => ({
    type: DEMANDE_GET_ALL,
});

export const demandesGetAllSuccess = (listRequests: IDemandes) : IDemandesGetAllSuccess => ({
    type: DEMANDE_GET_ALL__SUCCESS,
    payload:listRequests
});

export const demandesGetAllError = (errors:IErrors) : IDemandesGetAllError => ({
    type: DEMANDE_GET_ALL_ERROR,
    payload: errors,
});
