import { Action } from 'redux';
import { IErrors } from '@/utils/types';

export interface IDemande{
    id:string;
    name:string;
    beginDate: string;
    endDate:string,
    nbrDate:number;
    description:string;
}
export interface IDemandes{
    listDemandes: IDemande[];
}

/**
 * Create a Request
 */

export const DEMANDE_CREATE='DEMANDE_CREATE';
export type DEMANDE_CREATE = typeof DEMANDE_CREATE;

export const DEMANDE_CREATE_SUCCESS='DEMANDE_CREATE_SUCCESS';
export type DEMANDE_CREATE_SUCCESS = typeof DEMANDE_CREATE_SUCCESS;

export const DEMANDE_CREATE_ERROR='DEMANDE_CREATE_ERROR';
export type DEMANDE_CREATE_ERROR = typeof DEMANDE_CREATE_ERROR;

/**
 * Get All Request
 */

 export const DEMANDE_GET_ALL = 'DEMANDE_GET_ALL';
 export type DEMANDE_GET_ALL = typeof DEMANDE_GET_ALL;

 export const DEMANDE_GET_ALL__SUCCESS= 'DEMANDE_GET_ALL__SUCCESS';
 export type DEMANDE_GET_ALL__SUCCESS = typeof DEMANDE_GET_ALL__SUCCESS;

 export const DEMANDE_GET_ALL_ERROR = 'DEMANDE_GET_ALL_ERROR';
 export type DEMANDE_GET_ALL_ERROR = typeof DEMANDE_GET_ALL_ERROR;

 /**
 * Create a task to do : Action Creator
 */
export interface IDemandeCreate extends Action<DEMANDE_CREATE>{
    payload: IDemande;
}
export interface IDemandeCreateSuccess extends Action <DEMANDE_CREATE_SUCCESS>{}
export interface IDemandeCreateError extends Action<DEMANDE_CREATE_ERROR>{
    payload: IErrors;
}

/**
 get All Requests: Action creator
 */


 export interface IDemandeGetAll extends Action <DEMANDE_GET_ALL> {};
 export interface IDemandesGetAllSuccess extends Action <DEMANDE_GET_ALL__SUCCESS>{
     payload:IDemandes;
 }
 export interface IDemandesGetAllError extends Action <DEMANDE_GET_ALL_ERROR>{
     payload:IErrors;
 }

 export type IToDoActionType =
 IDemandeCreate
|IDemandeCreateSuccess
|IDemandeCreateError
|IDemandeGetAll
|IDemandesGetAllSuccess
|IDemandesGetAllError

