import React from 'react';
import styles from './demandesList.css';
import { Table } from 'antd';

export default () => {
  const columns = [
    { title: 'Nom', dataIndex: 'nom', key: 'nom' },
    { title: 'Date Debut', dataIndex: 'dateD', key: 'dateD' },
    { title: 'Date Fin', dataIndex: 'dateF', key: 'dateF' },
    { title: 'Nombre de jours', dataIndex: 'nbJ', key: 'nbJ' },
   
  ];
  
  const data = [
    {
      key: 1,
      nom: 'Sabri Gabtni',
      dateD: '24/04/2020',
      dateF: '28/04/2020',
      nbJ :2,
      type : "maladie"
    },
    {
      key: 2,
      nom: 'Fadhel DHahri',
      dateD: '20/04/2020',
      dateF: '21/04/2020',
      nbJ :1,
      type: "maladie"
  },
    {
      key: 3,
      nom: 'Amira Jhinaoui',
      dateD: '24/04/2020',
      dateF: '30/04/2020',
      nbJ :6,
      type: "Annuel"
    },
  ];
  return (
    
    <div>
      <h1 className={styles.title}>Page demandesList</h1>
      <h1>Liste des conges</h1>
                {/* <Content> */}
                <Table
              columns={columns}
               expandable={{
      expandedRowRender: (record:any) => <p style={{ margin: 0 }}>{record.type}</p>,
      rowExpandable: (record:any) => record.name !== 'Not Expandable',
    }}
    dataSource={data}
  />
                {/* </Content>
            </Layout> */}
           
        </div>
  
  );
}
